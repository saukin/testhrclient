
package me.saukin.testhrclient;

import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 *
 * @author S_SAUKIN
 */
public class TestHTTPClient {
    
    
    public static void main(String[] args) throws IOException {
        
        HttpClient client;
        client = new DefaultHttpClient();
        
        HttpGet request = new HttpGet("http://localhost:8080/HR_REST/webresources/me.saukin.hr_test_rest.countries/count");
        HttpResponse response = client.execute(request);
        
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        
        String line;
        
        while((line = rd.readLine()) != null) {
            System.out.println(line);
        }
        
    }
    
}
